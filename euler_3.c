/*
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
*/
#define TRUE 1
#define FALSE 0

#include <stdio.h>
#include <math.h>

int isPrime(long long unsigned divisor) {
    long long unsigned i;
    for(i=2; i < divisor; i++) {
        if(divisor % i == 0) {
            return FALSE;
        }
    } return TRUE;
}

long long largestPrime(long long unsigned input) {
    //long number;
    long long unsigned divisor;
    for(divisor = sqrt(input); divisor > 1; divisor--) {
        if(input % divisor == 0 && isPrime(divisor)) {
            return divisor;
        }
    }
}

void main() {
    printf("The largest prime factor of the 600851475143 = %llu", largestPrime(600851475143));
}
